class News {
  final String urlToImage;
  final String url;
  final String title;
  final String description;
  final String content;
  final dynamic source;

  News({
    this.content,
    this.description,
    this.source,
    this.title,
    this.url,
    this.urlToImage,
  });
}
