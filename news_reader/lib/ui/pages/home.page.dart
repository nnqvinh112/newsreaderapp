import 'package:flutter/material.dart';
import 'package:news_reader/ui/widgets/thumbnail-cart.widget.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool showSearchbar = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('News Reader'),
        centerTitle: true,
        actions: this.buildActions(context),
      ),
      body: this.buildBody(context),
    );
  }

  List<Widget> buildActions(context) {
    return [
      IconButton(
        icon: Icon(Icons.search),
        onPressed: () => this.setState(() {
          this.showSearchbar = true;
        }),
      ),
    ];
  }

  Widget buildBody(context) {
    return ListView(
      children: List.generate(10, (index) {
        return ThumbnailCard(
          title: 'News title 00$index',
          description:
              'This is the news description. It\'s length cannot be determined. This could be ver long or very short.',
          urlToImage:
              'https://image.shutterstock.com/image-vector/stock-vector-illustration-logo-breaking-260nw-688668343.jpg',
        );
      }),
    );
  }
}
