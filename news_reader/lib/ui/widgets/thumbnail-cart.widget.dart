import 'package:flutter/material.dart';

class ThumbnailCard extends StatelessWidget {
  final String title;
  final String description;
  final String url;
  final String urlToImage;

  const ThumbnailCard({
    Key key,
    this.description,
    this.title,
    this.url,
    this.urlToImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        // decoration: BoxDecoration(
        //   image: DecorationImage(image: NetworkImage(this.urlToImage)),
        // ),
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              this.title ?? '',
              style: Theme.of(context).textTheme.title,
            ),
            Text(
              this.description ?? '',
              style: Theme.of(context).textTheme.body1,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}
